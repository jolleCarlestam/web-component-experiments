class SogetiFetch {

	constructor(params = {
		contentType: 'application/json',
		mode: 'cors',
		cache: 'default'
	}) {
		this.params = params;
		this.headers = new Headers({
			'Content-Type': params.contentType
		});
		this.init = {
			headers: this.headers,
			mode: params.cors,
			cache: params.cache,
		}
	}

	async get (path = '', params = {}) {
		this.init['method'] = 'GET';
		const request = new Request(path, this.init);
		const response = await fetch(request);
		let result;
		switch (this.params.contentType) {
			case 'application/json':
				result = await response.json();
				break;

			default:
				result = await response.text();
				break;
		}
		return result;
	};

}

export default SogetiFetch;
