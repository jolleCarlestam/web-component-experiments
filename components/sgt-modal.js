customElements.define(
	'sgt-modal',
	class extends HTMLElement {
		get visible() {
			return this.hasAttribute('visible');
		}

		set visible(value) {
			if (value) {
				this.setAttribute('visible', '');
			} else {
				this.removeAttribute('visible');
			}
		}

		constructor() {
			super();
			this.shadow = this.attachShadow({ mode: 'open' });
		}

		connectedCallback() {

			this.keyListen = (e) => {
				if(e.key === "Escape" || (e.ctrlKey && e.key === '.')){
					this.removeAttribute('visible');
					this.dispatchEvent(new CustomEvent('cancel'));
				}
			}
			this._render();
			this._attachEventHandlers();
		}

		static get observedAttributes() {
			return ['visible'];
		}

		attributeChangedCallback(name, oldValue, newValue) {
			if (name === 'visible' && this.shadow) {
				if (newValue === null) {
					this.shadow.querySelector('.wrapper').classList.remove('visible');
					this.dispatchEvent(new CustomEvent('close'));
					window.document.removeEventListener('keydown', this.keyListen);
				} else {
					this.shadow.querySelector('.wrapper').classList.add('visible');
					this.dispatchEvent(new CustomEvent('open'));
					if (this.hasAttribute('allow-escape')) {
						window.document.addEventListener('keydown', this.keyListen);
					}
				}
			}
		}

		disconnectedCallback() {
			this._removeEventHandlers();
		}

		_render() {
			const wrapperClass = this.visible ? 'wrapper visible' : 'wrapper';
			const container = document.createElement('div');
			container.innerHTML = `
			  <style>
				.wrapper {
				  position: fixed;
				  left: 0;
				  top: 0;
				  width: 100%;
				  height: 100%;
				  background-color: var(--modal-wrapper-background-color, rgba(0,0,0,.75));
				  opacity: 0;
				  visibility: hidden;
				  transform: scale(1.1);
				  transition: visibility 0s linear .25s,opacity .25s 0s,transform .25s;
				  z-index: 1;
				}
				.visible {
				  opacity: 1;
				  visibility: visible;
				  transform: scale(1);
				  transition: visibility 0s linear 0s,opacity .25s 0s,transform .25s;
				}
				.modal {
				  font-family: var(--modal-font-family, Lato,'Helvetica Neue',Arial,Helvetica,sans-serif);
				  font-size: var(--modal-font-size, 1.2rem);
				  padding: 10px 10px 5px 10px;
				  background-color: var(--modal-background-color, #fff);
				  box-shadow: 7px 7px 7px 0 rgba(0,0,0,.2),1px 3px 15px 2px rgba(0,0,0,.2);
				  position: absolute;
				  top: 50%;
				  left: 50%;
				  transform: translate(-50%,-50%);
				  border-radius: 4px;
				  min-width: 300px;
				  max-height: 97vh;
				}
				.content {
					overflow: auto;
					max-height: calc(90vh - 10em);
				}
				.button-container {
				  margin-top: 0.6rem;
				  text-align: var(--modal-button-container-text-align, right);
				}
				button {
				  min-width: 80px;
				  background-color: #848e97;
				  border-color: #848e97;
				  border-style: solid;
				  border-radius: 2px;
				  padding: 3px;
				  color:white;
				  cursor: pointer;
				}
				button:hover, label.close-icon:hover {
				  background-color: #6c757d;
				  border-color: #6c757d;
				}
				label.close-icon {
					position: absolute;
					top: 0;
					right: 0;
					height: 30px;
					width: 30px;
					background: #666666;
					transition: all 200ms ease-in-out;
					display: inline-block;
					cursor: pointer;
					color: #fff;
					text-align: center;
					line-height: 30px;
				}
			  </style>
			  <div class="${wrapperClass}">
				<div class="modal">
					<span class="title"><slot name="title"></slot></span>
					<label class="close-icon">X</label>
					<div class="content">
						<slot></slot>
					</div>
					<div class="button-container">
						<slot name="actions"><button class="cancel cancel-btn">Close</Button></slot>
					</div>
				</div>
			  </div>`;

			this.shadow.appendChild(container);
		}

		_attachEventHandlers() {
			this.shadow.querySelector('.modal').addEventListener('click', e => e.stopPropagation());
			this.shadow.querySelector('.wrapper').addEventListener('click', this._hideModal.bind(this));
			this.shadow.querySelector('.close-icon').addEventListener('click', this._hideModal.bind(this));
			this.shadow.querySelector('.cancel-btn').addEventListener('click', this._hideModal.bind(this));
		}

		_removeEventHandlers() {
			this.shadow.querySelector('.close-icon').removeEventListener('click', this._hideModal.bind(this));
			this.shadow.querySelector('.cancel-btn').removeEventListener('click', this._hideModal.bind(this));
		}

		_hideModal() {
			this.removeAttribute('visible');
			this.dispatchEvent(new CustomEvent('cancel'));
		}
	}
);
