const template = document.createElement("template");
template.innerHTML = `<a href=""><slot /></a>`;

customElements.define(
  "sgt-link",
  class extends HTMLElement {
    get active() {
      return this.hasAttribute("active");
    }

    set active(value) {
      if (value) {
        this.setAttribute("active", "");
      } else {
        this.removeAttribute("active");
      }
    }

    href = this.getAttribute("href");
    constructor() {
      super();
      this.shadow = this.attachShadow({ mode: "open" });

      const style = document.createElement("style");

      style.textContent = `
			a {
				position: relative;
				vertical-align: middle;
				line-height: 1;
				text-decoration: none;
				flex: 0 0 auto;
				user-select: none;
				background: 0 0;
				padding: .92857143em 1.14285714em;
				text-transform: none;
				color: rgba(0,0,0,.87);
				box-shadow: none;
			    border: none;
			}
			a:hover {
				filter: invert(25%);
			}
			a.active {
				text-decoration: underline;
				text-underline-position: under;
				text-underline-offset: 0.3rem;
			}
		`;

      this.shadow.appendChild(style);
    }

    connectedCallback() {
      this.shadow.appendChild(template.content.cloneNode(true));
      this.link = this.shadow.querySelector("a");
      this.link.setAttribute("href", this.href);
      this.link.addEventListener("click", (e) => this.onClick(e));
      window.addEventListener("popstate", this.onLocationChange);
      if (window.location.pathname === this.href) {
        this.link.setAttribute("active", "");
        this.link.classList.add("active");
      }
    }

    disconnectedCallback() {
      window.removeEventListener("popstate", this.onLocationChange);
    }

    onLocationChange = () => {
      if (window.location.pathname === this.href) {
        this.link.setAttribute("active", "");
        this.link.classList.add("active");
      } else {
        this.link.removeAttribute("active");
        this.link.classList.remove("active");
      }
    };

    onClick = (event) => {
      event.preventDefault();
      window.history.pushState({}, "", this.href);
      const navEvent = new PopStateEvent("popstate");
      window.dispatchEvent(navEvent);
    };
  }
);
