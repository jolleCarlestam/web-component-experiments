const template = document.createElement('template');
template.innerHTML = `<div class="people-list__container">
<ul class="people-list__list"></ul>
</div>`;

customElements.define(
	'people-list',
	class extends HTMLElement {
		constructor() {
			super();
			this.shadow = this.attachShadow({ mode: 'open' });

			this.shadow.appendChild(template.content.cloneNode(true));

			// A private property that we'll use to keep track of list
			let _list = [];

			// Use defineProperty to define a prop on this object, ie, the component.
			// Whenever list is set, call render. This way when the parent component sets some data
			// on the child object, we can automatically update the child.
			Object.defineProperty(this, 'list', {
				get: () => _list,
				set: list => {
					_list = list;
					this.render();
				},
			});
		}

		render = () => {
			let ulElement = this.shadow.querySelector('.people-list__list');
			ulElement.innerHTML = '';

			this.list.forEach(person => {
				let li = this._createPersonListElement(person);
				ulElement.appendChild(li);
			});
		};

		_createPersonListElement = person => {
			let li = document.createElement('LI');
			li.innerHTML = person.name;
			li.className = 'people-list__name';
			li.onclick = () => {
				let event = new CustomEvent('PersonClicked', {
					detail: {
						personId: person.id,
					},
					bubbles: true,
				});
				this.dispatchEvent(event);
			};
			return li;
		};
	}
);
