const template = document.createElement('template');
template.innerHTML = `<div class="card__user-card-container">
  <h2 class="card__name">
	<span class="card__full-name"></span> (
	<span class="card__user-name"></span>)
  </h2>
  <p>Website: <a class="card__website"></a></p>
  <div class="card__hidden-content">
	<p class="card__address"></p>
  </div>
  <button class="card__details-btn">More Details</button>
</div>`;

customElements.define(
	'person-detail',
	class extends HTMLElement {
		constructor() {
			super();
			this.shadow = this.attachShadow({ mode: 'open' });

			const style = document.createElement('style');

			style.textContent = `
		.card__user-card-container {
			text-align: center;
			border-radius: 5px;
			border: 1px solid grey;
			font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
			margin: 3px;
		  }

		  .card__user-card-container:hover {
			box-shadow: 3px 3px 3px;
		  }

		  .card__hidden-content {
			display: none;
		  }

		  .card__details-btn {
			background-color: #dedede;
			padding: 6px;
			margin-bottom: 8px;
		  }
		`;

			this.shadow.appendChild(style);

			this.shadow.appendChild(template.content.cloneNode(true));

			// Setup a click listener on <user-card>
			this.addEventListener('click', e => {
				this.toggleCard();
			});
		}

		// Creating an API function so that other components can use this to populate this component
		updatePersonDetails(userData) {
			this.render(userData);
		}

		// Function to populate the card(Can be made private)
		render(userData) {
			this.shadow.querySelector('.card__full-name').innerHTML = userData.name;
			this.shadow.querySelector('.card__user-name').innerHTML = userData.username;
			this.shadow.querySelector('.card__website').innerHTML = userData.website;
			this.shadow.querySelector('.card__address').innerHTML = `<h4>Address</h4>
		${userData.address.suite}, <br />
		${userData.address.street},<br />
		${userData.address.city},<br />
		Zipcode: ${userData.address.zipcode}`;
		}

		toggleCard() {
			let elem = this.shadow.querySelector('.card__hidden-content');
			let btn = this.shadow.querySelector('.card__details-btn');
			btn.innerHTML = elem.style.display == 'none' ? 'Less Details' : 'More Details';
			elem.style.display = elem.style.display == 'none' ? 'block' : 'none';
		}
	}
);
