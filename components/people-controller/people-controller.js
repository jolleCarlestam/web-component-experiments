import './people-list.js';
import './person-detail.js';

const template = document.createElement('template');
template.innerHTML = `
  <people-list id="people-list"></people-list>
  <person-detail id="person-detail"></person-detail>
  `;

customElements.define(
	'people-controller',
	class extends HTMLElement {
		constructor() {
			super();
			this.peopleList = [];

			this.shadow = this.attachShadow({ mode: 'open' });

			const style = document.createElement('style');

			style.textContent = `
		  #people-list {
			width: 45%;
			display: inline-block;
		  }
		  #person-detail {
			width: 45%;
			display: inline-block;
		  }
 `;

			this.shadow.appendChild(style);

			this.shadow.appendChild(template.content.cloneNode(true));
		}
		connectedCallback() {
			this._fetchAndPopulateData();
		}

		_fetchAndPopulateData = () => {
			let peopleList = this.shadow.querySelector('#people-list');
			fetch(`https://jsonplaceholder.typicode.com/users`)
				.then(response => response.text())
				.then(responseText => {
					const list = JSON.parse(responseText);
					this.peopleList = list;
					peopleList.list = list;

					this._attachEventListener();
				})
				.catch(error => {
					console.error(error);
				});
		};

		_attachEventListener = () => {
			let personDetail = this.shadow.querySelector('#person-detail');

			//Initialize with person with id 1:
			personDetail.updatePersonDetails(this.peopleList[0]);

			this.shadow.addEventListener('PersonClicked', e => {
				// e contains the id of person that was clicked.
				// We'll find him using this id in the people list:
				personDetail.updatePersonDetails(this.peopleList.filter((person) => person.id === e.detail.personId)[0]);
			});

		}
	}
);
