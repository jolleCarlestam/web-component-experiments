const template = document.createElement('template');
template.innerHTML = `<slot />`;

customElements.define('sgt-route',
	class extends HTMLElement {

	path = this.getAttribute('path');

	constructor() {
		super();
		this.shadow = this.attachShadow({mode: 'open'});
	}
	onLocationChange = () => {
		if (window.location.pathname === this.path) {
			this.shadow.appendChild(template.content.cloneNode(true));
		} else {
			this.shadow.innerHTML = '';
		}
	};

	connectedCallback() {
		this.onLocationChange();
		window.addEventListener('popstate', this.onLocationChange);
	}

	disconnectedCallback() {
		window.removeEventListener('popstate', this.onLocationChange);
	};

});
