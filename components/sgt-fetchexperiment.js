import SogetiFetch from '../apis/sogeti.js';

const template = document.createElement('template');
template.innerHTML = `<div class="ui grid container">
	<div class="left aligned four wide column"> <div class="result"></div></div>
	<div class="center aligned four wide column"> <button class="ui button primary">Test Fetch</button></div>
</div>
`;

customElements.define(
	'sgt-fetchexperiment',
	class extends HTMLElement {
		constructor() {
			super();
			this.shadow = this.attachShadow({ mode: 'open' });

			const link = document.createElement('link');
			link.rel = 'stylesheet';
			link.href = 'https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css';
			this.shadow.appendChild(link);

			this.shadow.appendChild(template.content.cloneNode(true));
		}

		connectedCallback() {
			this.shadow.querySelector('button').addEventListener('click', this.onFetchClick);
		}

		disconnectedCallback() {
			this.shadow.querySelector('button').removeEventListener('click', this.onFetchClick);
		}

		onFetchClick = () => {
			const sogetiFetch = new SogetiFetch();
			sogetiFetch.get('/api/test.lasso').then(response => {
				console.log('response', response);
				this.shadow.querySelector('.result').innerHTML = JSON.stringify(response);
			});
		};
	}
);
