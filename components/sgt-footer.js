import './sgt-modal.js';

const template = document.createElement('template');
template.innerHTML = `<div class="ui grid container">
	<div class="left aligned six wide column"><a id="about-anchor">Web Component Experiments</a></div>
	<div class="center aligned four wide column"> <a id="license-anchor">License</a></div>
	<div class="right aligned six wide column"><a href="https://gitlab.com/jolleCarlestam/web-component-experiments/-/tree/main">Code repository</a></div>
</div>
<style>
a { cursor: pointer }
.about-modal {
	--modal-wrapper-background-color: rgba(77, 182, 133, 0.75);
	--modal-font-family: Times;
	--modal-font-size: 0.9rem;
	--modal-background-color: #8ebfd6;
	--modal-button-container-text-align: center;
}
.about-modal .modal-title {
	font-size: 2.8rem;
}
</style>
<sgt-modal id="license-modal" allow-escape>
	<h3 slot="title">License</h3>
	<article>

		<h1>The MIT License (MIT)</h1>

		<p>Copyright © 2021 &lt;Jolle Carlestam&gt;</p>

		<p>Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the “Software”), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:</p>

		<p>The above copyright notice and this permission notice shall be included in
		all copies or substantial portions of the Software.</p>

		<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
		THE SOFTWARE.</p>
	</article>
	<span slot="actions"><button class="ui button cancel-btn">OK, I got it!</Button></span>
</sgt-modal>

<sgt-modal class="about-modal" id="about-modal" allow-escape>
	<h3 class="modal-title" slot="title">About</h3>
	<article>

		<h1>About the Web Components Experiment site</h1>

		<p>This site is all about experimenting and presenting demo cases about <strong><a href="https://developer.mozilla.org/en-US/docs/Web/Web_Components">Web Components</a></strong>.</p>

		<p>It is probably under constant development and likely, for the most part, not stable.</p>

		<p>Site and code is maintained by <strong>Jolle Carlestam</strong> on his free time from all the fun work he does at <strong><a href="">Sogeti</a></strong>.</p>
	</article>
</sgt-modal>
`;

customElements.define(
	'sgt-footer',
	class extends HTMLElement {
		constructor() {
			super();
			this.shadow = this.attachShadow({ mode: 'open' });

			const link = document.createElement('link');
			link.rel = 'stylesheet';
			link.href = 'https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css';
			this.shadow.appendChild(link);

			this.shadow.appendChild(template.content.cloneNode(true));
		}

		connectedCallback() {
			this.aboutModal = this.shadow.querySelector('#about-modal');
			if (this.aboutModal) {
				this.aboutModal.addEventListener('cancel', function () {
					console.log('cancel event in about modal raised');
				});
			}
			this.shadow.querySelector('#about-anchor').addEventListener('click', this.onToggleAbout);
			this.licenseModal = this.shadow.querySelector('#license-modal');
			if (this.licenseModal) {
				this.licenseModal.addEventListener('cancel', function () {
					console.log('cancel event in license modal raised');
				});
				this.licenseModal.querySelector('.cancel-btn').addEventListener('click', this.onToggleLicense);
			}

			this.shadow.querySelector('#license-anchor').addEventListener('click', this.onToggleLicense);

		}

		disconnectedCallback() {
			this.shadow.querySelector('#license-anchor').removeEventListener('click', this.onToggleLicense);
			this.shadow.querySelector('#about-anchor').removeEventListener('click', this.onToggleAbout);
		}

		onToggleLicense = () => {
			this.licenseModal.visible = !this.licenseModal.visible;
		};
		onToggleAbout = () => {
			this.aboutModal.visible = !this.aboutModal.visible;
		};
	}
);
