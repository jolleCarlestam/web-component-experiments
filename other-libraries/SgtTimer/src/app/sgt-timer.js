customElements.define('sgt-timer',
	class extends HTMLElement {

	constructor() {
		super();
		this.shadow = this.attachShadow({mode: 'open'});

		const style = document.createElement('style');

		style.textContent = `
			:host {
				border: solid 1px gray;
				display: block;
				max-width: 80rem;
				padding: 1.6rem;
			}
			button {
				background-color: var(--button-background, limegreen);
				border: solid 1px;
				border-color: var(--button-border-color, lightseagreen);
				border-radius: 0.4rem;
				color: var(--button-color, white);
				padding: 0.8rem;
			}
			button.running {
				background-color: var(--button-background-running, royalblue);
				border: solid 1px var(--button-background-running, blue);
			}
		`;

		this.shadow.appendChild(style);

		this.wrapper = document.createElement('div');
		this.btnPara = document.createElement('p');
		this.labelPara = document.createElement('p');
		this.labelSpan = document.createElement('span');
		this.timerSpan = document.createElement('span');
		this.datePara = document.createElement('p');
		this.labelPara.appendChild(this.labelSpan);
		this.labelPara.appendChild(this.timerSpan);
		this.btn = document.createElement('button');
		this.btn.addEventListener('click', () => {this._toggleTimer()});
		this.btn.textContent = 'Start timer';
		this.btn.classList.add("stopped");
		this.btnPara.appendChild(this.btn);

		this.wrapper.appendChild(this.btnPara);
		this.wrapper.appendChild(this.labelPara);
		this.wrapper.appendChild(this.datePara);

		this.shadow.appendChild(this.wrapper);

	}

    connectedCallback() {
		this.labelSpan.textContent = this.getAttribute('label');
    }

	_pad(num) {
		return String(num).padStart(2, '0');
	}

	_formatDate(date) {
		return `${date.getFullYear()}-${this._pad(date.getMonth() + 1)}-${this._pad(date.getDate())} ${this._pad(date.getHours())}.${this._pad(date.getMinutes())}`;
	}

	_toggleTimer() {
		if(this.runningTimer) {
			clearInterval(this.runningTimer);
			this.runningTimer = 0;
			this.labelSpan.textContent = 'Deed is done: ';
			this.btn.textContent = 'Start timer';
			this.btn.classList.remove("running");
			this.btn.classList.add("stopped");
			this.stopDate = new Date();
			this.onChange('stopped');
		} else {
			this.startDate = new Date();
			this.datePara.textContent = this._formatDate(this.startDate);
			this.startTime = this.startDate.getTime();
			this.stopDate = null;
			this.labelSpan.textContent = 'Timer: ';
			this.btn.textContent = 'Stop timer';
			this.btn.classList.remove("stopped");
			this.btn.classList.add("running");
			this.runningTimer = setInterval(() => {
				const now = new Date().getTime();
				const distance = now - this.startTime;
				const days = Math.floor(distance / (1000 * 60 * 60 * 24));
				const hours = this._pad(
				Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
				);
				const minutes = this._pad(
				Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
				);
				const seconds = this._pad(Math.floor((distance % (1000 * 60)) / 1000));
				this.timerSpan.textContent = `${days}: ${hours}: ${minutes}: ${seconds}`;
			}, 500);
			this.onChange('started');

		}

	}

	onChange(action) {
		console.log('onChange called in component');
		this.dispatchEvent(
			new CustomEvent('onchange', {
				detail: {
					action: action,
					start: this.startDate,
					stop: this.stopDate,
				},
				bubbles: true,
				composed: true,
			})
		);
	}

});
