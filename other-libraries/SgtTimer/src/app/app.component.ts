import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  handleChange(event: { detail: any; } | any) {
    console.log('handleChange in Angular', event.detail);
  }

}
