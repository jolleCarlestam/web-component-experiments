import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import '../../../../components/sgt-timer';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule],
  providers: [],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA, // Tells Angular there're custom tags (Web Components) in our templates
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
