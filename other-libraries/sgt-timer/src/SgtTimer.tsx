import React, { useLayoutEffect, useRef } from 'react';
import '../../../components/sgt-timer';

declare global {
	namespace JSX {
		interface IntrinsicElements {
			'sgt-timer': SgtTimerProps;
		}
	}
}

interface SgtTimerProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement> {
	label?: string;
	onchange?: any;
}

/* eslint-disable react-hooks/exhaustive-deps */

export const SgtTimer = (props: any) => {
	const ref = useRef<HTMLElement>(null);

	useLayoutEffect(() => {
		const handleChange = (customEvent: any) => props.onchange(customEvent);

		const { current } = ref;

		if (null !== ref) {
			current?.addEventListener('onchange', handleChange);
		}

		return () => current?.removeEventListener('onchange', handleChange);
	}, [ref]);

	return <sgt-timer ref={ref} label={props.label} />;
};
