import React from 'react';
import { SgtTimer } from './SgtTimer';

function App() {
	const handleChange: any = (e: { detail: any }) => {
		console.log('React says', e.detail);
	};

	return <SgtTimer label="Click button to start the React timer" onchange={(e: any) => handleChange(e)} />;
}

export default App;
