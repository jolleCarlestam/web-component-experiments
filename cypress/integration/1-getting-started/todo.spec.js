/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe("local web components experiments", () => {
  beforeEach(() => {
    // Cypress starts out with a blank slate for each test
    // so we must tell it to visit our website with the `cy.visit()` command.
    // Since we want to visit the same URL at the start of all our tests,
    // we include it in our beforeEach function so that it runs before each test
    cy.visit("http://wc.sogeti.test/");
  });

  it("displays six link components in header", () => {
    // We use the `cy.get()` command to get all elements that match the selector.
    // Then, we use `should` to assert that there are six matched items,
    cy.get("sgt-header sgt-link").should("have.length", 6);
  });

  it("navigates to Plain", () => {
    // We use the `cy.get()` command to get all elements that match the selector.
    // Then, we use `should` to assert that there are six matched items,
    cy.get("sgt-header sgt-link").eq(1).click();
    cy.url().should("include", "/plain");
  });
  it("starts timer in Plain", () => {
    // We use the `cy.get()` command to get all elements that match the selector.
    // Then, we use `should` to assert that there are six matched items,
    cy.get("sgt-header sgt-link").eq(1).click();
    cy.url().should("include", "/plain");
    cy.get("#sgt-timer").eq(0).should("have.length", 1);
    cy.get(".stopped").click();
    //cy.get("sgt-timer button").eq(0).click().should("hasClass", "running");
  });
});
